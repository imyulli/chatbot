const restify = require('restify');
const builder = require('botbuilder');
const intents = new builder.IntentDialog();

const dialogs = {
  activation: require('./app/dialogs/activation')
};


//=========================================================
// Bot Setup
//=========================================================

// Setup Restify Server
const server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
  console.log('%s listening to %s', server.name, server.url);
});

// Create chat bot
const connector = new builder.ChatConnector({
  appId: process.env.MICROSOFT_APP_ID,
  appPassword: process.env.MICROSOFT_APP_PASSWORD
});
const bot = new builder.UniversalBot(connector);
server.post('/api/messages', connector.listen());

//=========================================================
// Bots Dialogs
//=========================================================

bot.dialog('/', intents);
dialogs.activation(bot, builder);

intents.onDefault([
  function (session) {
    builder.Prompts.choice(session, 'Hello, how can I help you?', {activation: 'activation', login: 'login help'});
  },
  function(session, results) {
    if(results.response.entity) {
      session.beginDialog(`/${results.response.entity}`);
    } else {
      session.endDialog();
    }
  }
]);