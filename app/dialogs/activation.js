module.exports = function(bot, builder) {
  bot.dialog('/activation', [
    function(session) {
      builder.Prompts.text(session, 'What is your email address?');
    },
    function(session, results) {
      session.userData.email = results.response;
      builder.Prompts.text(session, 'What is your last name?');
    },
    function(session) {
      session.send('Give me a minute please, I\'ll send you an email');
    }
  ]);
};